// Copyright (c) 2010 Satoshi Nakamoto
// Copyright (c) 2009-2012 The Bitcoin developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "assert.h"

#include "chainparams.h"
#include "main.h"
#include "util.h"

#include <boost/assign/list_of.hpp>

using namespace boost::assign;

struct SeedSpec6 {
    uint8_t addr[16];
    uint16_t port;
};

#include "chainparamsseeds.h"

//
// Main network
//

// Convert the pnSeeds6 array into usable address objects.
static void convertSeed6(std::vector<CAddress> &vSeedsOut, const SeedSpec6 *data, unsigned int count)
{
    // It'll only connect to one or two seed nodes because once it connects,
    // it'll get a pile of addresses with newer timestamps.
    // Seed nodes are given a random 'last seen time' of between one and two
    // weeks ago.
    const int64_t nOneWeek = 7*24*60*60;
    for (unsigned int i = 0; i < count; i++)
    {
        struct in6_addr ip;
        memcpy(&ip, data[i].addr, sizeof(ip));
        CAddress addr(CService(ip, data[i].port));
        addr.nTime = GetTime() - GetRand(nOneWeek) - nOneWeek;
        vSeedsOut.push_back(addr);
    }
}

class CMainParams : public CChainParams {
public:
    CMainParams() {
        // The message start string is designed to be unlikely to occur in normal data.
        // The characters are rarely used upper ASCII, not valid as UTF-8, and produce
        // a large 4-byte int at any alignment.
        pchMessageStart[0] = 0x48;
        pchMessageStart[1] = 0x49;
        pchMessageStart[2] = 0x56;
        pchMessageStart[3] = 0x45;
        vAlertPubKey = ParseHex("04d6a7aaf631afe0b65dde0363dc11ad7cdc05f30a692d16b7c96f2af69efcace9af8581ebfe8edc54b4cdef15ff0dfc204da9780aec107c6f08eac3a11c992bfe");
        nDefaultPort = 17170;
        nRPCPort = 17171;
        bnProofOfWorkLimit = CBigNum(~uint256(0) >> 1);

        const char* pszTimestamp = " Thu, 24 Sep 2015 10:56:37 GMT #0000000000000000078789b5d9dc48ab33fd72a2bc5aaba370ea9b9497461da6";
        std::vector<CTxIn> vin;
        vin.resize(1);
        vin[0].scriptSig = CScript() << 0 << CBigNum(42) << vector<unsigned char>((const unsigned char*)pszTimestamp, (const unsigned char*)pszTimestamp + strlen(pszTimestamp));
        std::vector<CTxOut> vout;
        vout.resize(1);
        vout[0].SetEmpty();
        CTransaction txNew(1, 1443092197, vin, vout, 0);
        genesis.vtx.push_back(txNew);
        genesis.hashPrevBlock = 0;
        genesis.hashMerkleRoot = genesis.BuildMerkleTree();
        genesis.nVersion = 1;
        genesis.nTime    = 1443092197;
        genesis.nBits    = bnProofOfWorkLimit.GetCompact();
        genesis.nNonce   = 20150924;

        hashGenesisBlock = genesis.GetHash();
		

        assert(hashGenesisBlock == uint256("0xaf82d07152de919260f0db4ac90aed864b8330dfa791a311a1accbb01d47e2a9"));
        assert(genesis.hashMerkleRoot == uint256("0x07438503782e4a5f35a915255fbb339f97fc083c9f478afc95343c659995584a"));

        vSeeds.push_back(CDNSSeedData("45.63.67.234", "45.63.67.234"));
        vSeeds.push_back(CDNSSeedData("45.32.237.218", "45.32.237.218"));
		
        base58Prefixes[PUBKEY_ADDRESS] = list_of(40); // H
        base58Prefixes[SCRIPT_ADDRESS] = list_of(85);
        base58Prefixes[SECRET_KEY] =     list_of(153);
        base58Prefixes[EXT_PUBLIC_KEY] = list_of(0x04)(0x88)(0xB2)(0x1E);
        base58Prefixes[EXT_SECRET_KEY] = list_of(0x04)(0x88)(0xAD)(0xE4);

        convertSeed6(vFixedSeeds, pnSeed6_main, ARRAYLEN(pnSeed6_main));

    }

    virtual const CBlock& GenesisBlock() const { return genesis; }
    virtual Network NetworkID() const { return CChainParams::MAIN; }

    virtual const vector<CAddress>& FixedSeeds() const {
        return vFixedSeeds;
    }
protected:
    CBlock genesis;
    vector<CAddress> vFixedSeeds;
};
static CMainParams mainParams;


//
// Testnet
//

class CTestNetParams : public CMainParams {
public:
    CTestNetParams() {
        // The message start string is designed to be unlikely to occur in normal data.
        // The characters are rarely used upper ASCII, not valid as UTF-8, and produce
        // a large 4-byte int at any alignment.
        pchMessageStart[0] = 0x3f;
        pchMessageStart[1] = 0x1a;
        pchMessageStart[2] = 0x1c;
        pchMessageStart[3] = 0x05;
        bnProofOfWorkLimit = CBigNum(~uint256(0) >> 1);
        vAlertPubKey = ParseHex("04dd70b786aae17f2bd49012b97373a86fde4958f60e3fe042ee235bf7b562f5354c99cfdea895d526b88fb00cd5b98541a19b0aff8b5d9ab620bd62b2ba76e21a");
        nDefaultPort = 27270;
        nRPCPort = 27271;
        strDataDir = "testnet";

        // Modify the testnet genesis block so the timestamp is valid for a later start.
        genesis.nBits  = bnProofOfWorkLimit.GetCompact();
        genesis.nNonce = 753753;

        hashGenesisBlock = genesis.GetHash();
		
		
        assert(hashGenesisBlock == uint256("0xc1378a3d6e9334ab43192a52c97a3bda7066e19d17727d7fdf750faf7ef1b698"));

        vFixedSeeds.clear();
        vSeeds.clear();

        base58Prefixes[PUBKEY_ADDRESS] = list_of(83);
        base58Prefixes[SCRIPT_ADDRESS] = list_of(196);
        base58Prefixes[SECRET_KEY]     = list_of(239);
        base58Prefixes[EXT_PUBLIC_KEY] = list_of(0x04)(0x35)(0x87)(0xCF);
        base58Prefixes[EXT_SECRET_KEY] = list_of(0x04)(0x35)(0x83)(0x94);

        convertSeed6(vFixedSeeds, pnSeed6_test, ARRAYLEN(pnSeed6_test));

    }
    virtual Network NetworkID() const { return CChainParams::TESTNET; }
};
static CTestNetParams testNetParams;

static CChainParams *pCurrentParams = &mainParams;

const CChainParams &Params() {
    return *pCurrentParams;
}

void SelectParams(CChainParams::Network network) {
    switch (network) {
        case CChainParams::MAIN:
            pCurrentParams = &mainParams;
            break;
        case CChainParams::TESTNET:
            pCurrentParams = &testNetParams;
            break;
        default:
            assert(false && "Unimplemented network");
            return;
    }
}

bool SelectParamsFromCommandLine() {
    bool fTestNet = GetBoolArg("-testnet", false);

    if (fTestNet) {
        SelectParams(CChainParams::TESTNET);
    } else {
        SelectParams(CChainParams::MAIN);
    }
    return true;
}
